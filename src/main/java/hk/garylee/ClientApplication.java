package hk.garylee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientApplication {
    private final Socket socket;
    private final PrintWriter printWriter;
    private final BufferedReader reader;

    public ClientApplication(final String host, final int port) throws IOException {
        socket = new Socket(host, port);
        printWriter = new PrintWriter(socket.getOutputStream(), true);

        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        // start reader thread
        new Thread(() -> {
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    println(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void close() throws IOException {
        printWriter.close();
        reader.close();
    }

    public void send(final String line) {
        printWriter.println(line);
    }

    public void println(final String line) {
        System.out.println(line);
    }

    public static void main(String[] args) throws IOException {
        final ClientApplication clientApplication = new ClientApplication("localhost", 8000);

        String line;
        BufferedReader userInputReader = new BufferedReader(new InputStreamReader(System.in));
        while ((line = userInputReader.readLine()) != null) {
            clientApplication.send(line);
        }
    }
}
