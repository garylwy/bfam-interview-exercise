package hk.garylee.model;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

@Getter
public class PriceBook {
    private final int securityId;
    private final HashMap<Double, PriceNode> hashMap = new HashMap<>();
    private final TreeMap<Double, PriceNode> treeMap = new TreeMap<>();

    private PriceNode highest;
    private PriceNode lowest;

    private final HashMap<String, SecuritySourceNode> securitySourceMap = new HashMap<>();

    public PriceBook(final int securityId) {
        this.securityId = securityId;
    }

    public Optional<SecuritySourceNode> getSecuritySourceNode(final String securitySourceKey) {
        return Optional.ofNullable(securitySourceMap.get(securitySourceKey));
    }

    public void add(final PriceNode node) {
        if (node == null) {
            return;
        }

        if (highest == null) {
            highest = lowest = node;
        } else if (node.getPrice() > highest.getPrice()) {
            highest = node;
        } else if (node.getPrice() < highest.getPrice()) {
            lowest = node;
        }

        hashMap.put(node.getPrice(), node);
        treeMap.put(node.getPrice(), node);     // O(log m)
    }

    public void remove(final PriceNode node) {
        if (node == null) {
            return;
        }

        if (highest == lowest) {
            highest = lowest = null;
        } else if (node == highest) {
            highest = Optional.ofNullable(treeMap.lowerEntry(node.getPrice())).map(Map.Entry::getValue).orElse(null);
        } else if (node == lowest) {
            lowest = Optional.ofNullable(treeMap.higherEntry(node.getPrice())).map(Map.Entry::getValue).orElse(null);
        }

        hashMap.remove(node.getPrice());
        treeMap.remove(node.getPrice());    // O(log m)
    }

    public void add(final double price, final SecuritySourceNode node) {
        if (node == null) {
            return;
        }

        securitySourceMap.put(node.getKey(), node);

        final PriceNode priceNode = hashMap.get(price);
        if (priceNode != null) {
            priceNode.add(node);        // O(1)
            return;
        }

        add(new PriceNode(price));
    }

    public void remove(final SecuritySourceNode node) {
        if (node == null) {
            return;
        }

        securitySourceMap.remove(node.getKey());
        node.getParent().remove(node);
    }
}
