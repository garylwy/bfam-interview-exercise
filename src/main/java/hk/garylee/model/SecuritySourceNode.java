package hk.garylee.model;

import lombok.Getter;
import lombok.Setter;

@Getter
public class SecuritySourceNode {
    private int securityId;
    private int sourceId;
    @Setter
    private PriceNode parent;
    SecuritySourceNode prev;
    SecuritySourceNode next;

    public SecuritySourceNode(final int securityId, final int sourceId) {
        this.securityId = securityId;
        this.sourceId = sourceId;
    }

    public String getKey() {
        return securityId + "-" + sourceId;
    }
}
