package hk.garylee.model;

import lombok.Getter;

@Getter
public class PriceNode {
    private final double price;

    private SecuritySourceNode head;
    private SecuritySourceNode tail;

    public PriceNode(final double price) {
        this.price = price;
    }

    public void add(final SecuritySourceNode node) {
        if (node == null) {
            return;
        }

        node.setParent(this);

        if (head == null) {
            head = tail = node;
        } else {
            tail.next = node;
            node.prev = tail;
            tail = node;
        }
    }

    public void remove(final SecuritySourceNode node) {
        if (node == null) {
            return;
        }

        if (head == tail) {
            head = tail = null;
        } else if (node == head) {
            head = head.next;
            if (head != null) {
                head.prev = null;
            }
            node.next = null;
        } else if (node == tail) {
            tail = tail.prev;
            tail.next = null;
            node.prev = null;
        } else  {
            node.prev.next = node.next;
            node.next.prev = node.prev;
        }
    }

    public boolean isEmpty() {
        return head == null;
    }
}
