package hk.garylee.marketmarker.server;

import hk.garylee.service.quotecalculation.QuoteCalculationService;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.net.SocketAddress;

@Data
@NoArgsConstructor
public class MarketMakerServerConfig {
    SocketAddress bindAddress;
    int backlog = 100;
    int readBufferSize = 1024;

    int acceptableThreadPoolSize = 4;
    int readableThreadPoolSize = 4;

    QuoteCalculationService quoteCalculationService;

    boolean blockingMode = false;

    public MarketMakerServerConfig(final SocketAddress bindAddress) {
        this.bindAddress = bindAddress;
    }

}
