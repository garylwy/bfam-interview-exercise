package hk.garylee.marketmarker.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public class MarketMakerNonBlockingServer extends Thread implements MarketMakerServer {
    private MarketMakerServerConfig config;
    private ServerSocketChannel serverSocketChannel;

    private Selector selector;

    private boolean running;

    public MarketMakerNonBlockingServer(final MarketMakerServerConfig config) throws IOException {
        this.config = config;

        selector = Selector.open();
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.bind(config.bindAddress, config.backlog);
    }

    public void close() throws IOException {
        running = false;
        selector.close();
        serverSocketChannel.close();
    }

    @Override
    public void run() {
        running = true;

        try {
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            while (running) {
                selector.select();

                final Set<SelectionKey> selectionKeys = selector.selectedKeys();
                selectionKeys.forEach(this::processSelectedKey);
                selectionKeys.clear();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processSelectedKey(final SelectionKey key) {
        if (key == null) {
            return;
        }

        try {
            if (key.isAcceptable()) {
                processAcceptableKey(key);
            } else if (key.isReadable()) {
                processReadableKey(key);
            }
        } catch (IOException e) {
            key.cancel();
        }
    }

    private void processAcceptableKey(final SelectionKey key) throws IOException {
        if (key == null) {
            return;
        }

        final SocketChannel socketChannel = ((ServerSocketChannel) key.channel()).accept();
        if (socketChannel != null) {
            socketChannel.configureBlocking(false);

            // DirectBuffer is used for avoid memory coping from or to OS's I/O operation.
            socketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocateDirect(config.readBufferSize));
        }
    }

    private void processReadableKey(final SelectionKey key) throws IOException {
        if (key == null) {
            return;
        }

        final ByteBuffer buffer = (ByteBuffer) key.attachment();
        final SocketChannel socketChannel = (SocketChannel) key.channel();
        final int len = socketChannel.read(buffer);
        if (len <= 0) {
            key.cancel();
            return;
        }

        Optional<byte[]> data;
        while ((data = MarketMakerServer.read(buffer)).isPresent()) {
            final byte[] bytes = data.get();
            final CompletableFuture<OptionalDouble> future = MarketMakerServer.processLine(config.quoteCalculationService, bytes);
            MarketMakerServer.processCalculatedPrice(future, socketChannel);
        }
    }
}
