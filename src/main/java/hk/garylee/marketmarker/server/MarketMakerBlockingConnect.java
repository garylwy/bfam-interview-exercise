package hk.garylee.marketmarker.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.concurrent.CompletableFuture;

public class MarketMakerBlockingConnect extends Thread implements MarketMakerServer {
    private SocketChannel socketChannel;
    private MarketMakerServerConfig config;

    private ByteBuffer readBuffer;


    public MarketMakerBlockingConnect(final SocketChannel socketChannel, final MarketMakerServerConfig config) {
        this.socketChannel = socketChannel;
        this.config = config;

        // DirectBuffer is used for avoid memory coping from or to OS's I/O operation.
        readBuffer = ByteBuffer.allocateDirect(config.readBufferSize);
    }

    @Override
    public void run() {
        try {
            Optional<byte[]> data;
            while (socketChannel.read(readBuffer) >= 0) {
                while ((data = MarketMakerServer.read(readBuffer)).isPresent()) {
                    final CompletableFuture<OptionalDouble> future = MarketMakerServer.processLine(config.quoteCalculationService, data.get());
                    MarketMakerServer.processCalculatedPrice(future, socketChannel);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
