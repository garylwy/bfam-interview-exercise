package hk.garylee.marketmarker.server;

import hk.garylee.service.quotecalculation.QuoteCalculationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;

public interface MarketMakerServer {

    static MarketMakerServer create(MarketMakerServerConfig config) throws IOException {
        if (config == null) {
            return null;
        }

        return config.isBlockingMode() ? new MarketMakerBlockingServer(config) : new MarketMakerNonBlockingServer(config);
    }

    /**
     * Read line (until \r or \n) from ByteBuffer.
     *
     * @param readBuffer
     * @return
     */
    static Optional<byte[]> read(final ByteBuffer readBuffer) {
        if (readBuffer == null) {
            return Optional.empty();
        }

        readBuffer.flip();
        readBuffer.mark();
        byte b;
        while (readBuffer.hasRemaining()) {
            b = readBuffer.get();

            if (b == '\r' || b == '\n') {
                final byte[] data = new byte[readBuffer.position() - 1];
                if (data.length > 0) {
                    readBuffer.reset();
                    readBuffer.get(data);
                    readBuffer.position(readBuffer.position() + 1);

                    // skip new line separator
                    while (readBuffer.hasRemaining()) {
                        readBuffer.mark();
                        b = readBuffer.get();
                        if (b != '\r' && b != '\n') {
                            readBuffer.reset();
                            break;
                        }
                    }

                    readBuffer.compact();
                    return Optional.of(data);
                }
            }
        }
        readBuffer.reset();
        readBuffer.compact();
        return Optional.empty();
    }

    /**
     * This method responsible to line read from ByteBuffer. It including unpacking message and calculate price.
     * Since calculateQuotePrice method may takes long time, this method return Future object.
     * But it may cause the respond ordering NOT matching with request ordering.
     * This problem can be solve by introducing messageId in both client and server side.
     *
     * @param quoteCalculationService
     * @param data
     * @return
     */
    static CompletableFuture<OptionalDouble> processLine(final QuoteCalculationService quoteCalculationService, final byte[] data) {
        CompletableFuture<OptionalDouble> future = new CompletableFuture<>();

        if (quoteCalculationService == null || data == null || data.length == 0) {
            future.complete(OptionalDouble.empty());
        } else {
            ForkJoinPool.commonPool().submit(() -> {
                final String[] split = new String(data).split(" ");
                if (split.length == 3) {
                    final int securityId = Integer.parseInt(split[0]);
                    final boolean buy = split[1].equals("BUY");
                    final int quantity = Integer.parseInt(split[2]);

                    future.complete(quoteCalculationService.calculateQuotePrice(securityId, buy, quantity));
                } else {
                    future.complete(OptionalDouble.empty());
                }
            });
        }

        return future;
    }

    /**
     * Sending response to client based on calculated price. Responding -1 if price is not available at that moment.
     *
     * @param future
     * @param socketChannel
     */
    static void processCalculatedPrice(final CompletableFuture<OptionalDouble> future, final SocketChannel socketChannel) {
        if (future == null || socketChannel == null) {
            return;
        }

        future.thenAccept(price -> {
            try {
                if (price.isPresent()) {
                    MarketMakerServer.response(socketChannel, price.getAsDouble());
                } else {
                    MarketMakerServer.response(socketChannel, -1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    static void response(final SocketChannel socketChannel, final double price) throws IOException {
        if (socketChannel == null) {
            return;
        }

        String response = price + "\n";
        byte[] data = response.getBytes();
        ByteBuffer buffer = ByteBuffer.allocateDirect(data.length);
        buffer.put(data);
        buffer.flip();

        socketChannel.write(buffer);
    }

    void start();
}
