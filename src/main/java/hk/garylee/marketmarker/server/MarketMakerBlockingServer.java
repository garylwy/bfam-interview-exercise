package hk.garylee.marketmarker.server;

import java.io.IOException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class MarketMakerBlockingServer extends Thread implements MarketMakerServer {
    private MarketMakerServerConfig config;

    private ServerSocketChannel serverSocketChannel;

    public MarketMakerBlockingServer(final MarketMakerServerConfig config) throws IOException {
        this.config = config;

        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(config.bindAddress, config.backlog);
    }

    @Override
    public void run() {
        SocketChannel socketChannel;

        try {
            while ((socketChannel = serverSocketChannel.accept()) != null) {
                new MarketMakerBlockingConnect(socketChannel, config).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
