package hk.garylee.service.quotecalculation;

import com.example.marketmaker.QuoteCalculationEngine;

/**
 * This engine simply markup/markdown based on side (BUY/SELL) and reference price
 */
public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    private double markup;

    public QuoteCalculationEngineImpl(final double markup) {
        this.markup = markup;
    }

    public double getMarkup() {
        return markup;
    }

    @Override
    public double calculateQuotePrice(final int securityId, final double referencePrice, final boolean buy, final int quantity) {
        return referencePrice * (1 + (buy ? 1 : -1) * markup);
    }
}
