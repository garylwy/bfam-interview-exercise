package hk.garylee.service.quotecalculation;

import com.example.marketmaker.QuoteCalculationEngine;
import hk.garylee.service.referenceprice.ReferencePriceService;

import java.util.OptionalDouble;

public class QuoteCalculationServiceImpl implements QuoteCalculationService {
    private final QuoteCalculationEngine quoteCalculationEngine;
    private final ReferencePriceService referencePriceService;

    public QuoteCalculationServiceImpl(final QuoteCalculationEngine quoteCalculationEngine,
                                       final ReferencePriceService referencePriceService) {

        if (quoteCalculationEngine == null) throw new NullPointerException("QuoteCalculationEngine is null");
        if (referencePriceService == null) throw new NullPointerException("ReferencePriceService is null");

        this.quoteCalculationEngine = quoteCalculationEngine;
        this.referencePriceService = referencePriceService;
    }


    @Override
    public OptionalDouble calculateQuotePrice(final int securityId, final boolean buy, final int quantity) {
        final OptionalDouble bestPrice = referencePriceService.getBestPrice(securityId, buy);
        if (!bestPrice.isPresent()) {
            return OptionalDouble.empty();
        }

        return OptionalDouble.of(quoteCalculationEngine.calculateQuotePrice(securityId, bestPrice.getAsDouble(), buy, quantity));
    }
}
