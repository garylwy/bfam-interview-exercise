package hk.garylee.service.quotecalculation;

import java.util.OptionalDouble;

public interface QuoteCalculationService {
    /**
     * Calculate price for quote request on a security
     *
     * @param securityId
     * @param buy
     * @param quantity
     * @return
     */
    OptionalDouble calculateQuotePrice(int securityId, boolean buy, int quantity);
}
