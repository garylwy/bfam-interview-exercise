package hk.garylee.service.referenceprice;

import com.example.marketmaker.ReferencePriceSource;
import hk.garylee.model.PriceBook;
import hk.garylee.model.PriceNode;
import hk.garylee.model.SecuritySourceNode;

import java.util.HashMap;
import java.util.Optional;
import java.util.OptionalDouble;

/**
 * This implementation of ReferencePriceService is aimed to demonstrate using the tree and linked-list data structure to
 * provide reference price service with fast update and retrieve best price over multiple sources
 *
 * Price update involved removing old price and adding new price.
 * Removing new price:
 * If no other source having price equal to the old price, time complexity is O(log m). Otherwise, O(1).
 *
 * Adding new price:
 * if no other source having price equal to the new price, time complexity is o(log m). Otherwise, O(1).
 *
 * Get best price: O(1)
 *
 * Please note that this implementation is not thread-safe. But it can be if spend some effort.
 */
public class ReferencePriceServiceImplV2 implements ReferencePriceService {
    private final HashMap<Integer, PriceBook> bookMap = new HashMap<>();

    @Override
    public void addPriceSource(final ReferencePriceSource source) {
        source.subscribe(((securityId, price) -> {
            final PriceBook priceBook = bookMap.computeIfAbsent(securityId, secId -> new PriceBook(securityId));

            final String securitySourceKey = securityId + "-" + source.getSourceId();
            final Optional<SecuritySourceNode> securitySourceNode = priceBook.getSecuritySourceNode(securitySourceKey);
            if (securitySourceNode.isPresent()) {
                final SecuritySourceNode node = securitySourceNode.get();
                priceBook.remove(node);                     // O(1)

                if (node.getParent().isEmpty()) {
                    priceBook.remove(node.getParent());     // O(log m)
                }
            }

            final SecuritySourceNode node = new SecuritySourceNode(securityId, source.getSourceId());
            priceBook.add(price, node);                     // O(1) or O(log m)
        }));
    }

    @Override
    public void removePriceSource(final ReferencePriceSource source) {
        source.stopSubscribe();
    }

    @Override
    public OptionalDouble getBestBidPrice(final int securityId) {
        return Optional.ofNullable(bookMap.get(securityId))
                .map(PriceBook::getHighest)
                .map(PriceNode::getPrice)
                .map(OptionalDouble::of)
                .orElseGet(OptionalDouble::empty);
    }

    @Override
    public OptionalDouble getBestAskPrice(final int securityId) {
        return Optional.ofNullable(bookMap.get(securityId))
                .map(PriceBook::getLowest)
                .map(PriceNode::getPrice)
                .map(OptionalDouble::of)
                .orElseGet(OptionalDouble::empty);
    }

    @Override
    public OptionalDouble getBestPrice(final int securityId, final boolean buy) {
        return buy ? getBestAskPrice(securityId) : getBestBidPrice(securityId);
    }
}
