package hk.garylee.service.referenceprice;

import com.example.marketmaker.ReferencePriceSource;

import java.util.OptionalDouble;

/**
 * Service for maintaining price from multiple sources
 */
public interface ReferencePriceService {
    /**
     * Add price source
     * @param source
     */
    void addPriceSource(final ReferencePriceSource source);

    /**
     * Remove price source
     * @param source
     */
    void removePriceSource(final ReferencePriceSource source);

    /**
     * Get the highest bid price across all price sources
     *
     * @param securityId
     * @return the highest bid price
     */
    OptionalDouble getBestBidPrice(final int securityId);

    /**
     * Get the lowest ask price across all price sources
     *
     * @param securityId
     * @return the lowest ask price
     */
    OptionalDouble getBestAskPrice(final int securityId);

    /**
     * Get the best price for buy/sell side
     *
     * @param securityId
     * @param buy
     * @return
     */
    OptionalDouble getBestPrice(final int securityId, final boolean buy);
}
