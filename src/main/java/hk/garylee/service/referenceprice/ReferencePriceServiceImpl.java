package hk.garylee.service.referenceprice;

import com.example.marketmaker.ReferencePriceSource;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class ReferencePriceServiceImpl implements ReferencePriceService {
    private final List<ReferencePriceSource> sources = new CopyOnWriteArrayList<>();
    private final Map<Integer, Map<ReferencePriceSource, Double>> securityPriceMap = new ConcurrentHashMap<>();

    public void addPriceSource(final ReferencePriceSource source) {
        if (source == null) {
            return;
        }

        source.subscribe((securityId, price) -> {
            final Map<ReferencePriceSource, Double> map = securityPriceMap.computeIfAbsent(securityId, secId -> new ConcurrentHashMap<>());
            map.put(source, price);
        });
        sources.add(source);
    }

    @Override
    public void removePriceSource(final ReferencePriceSource source) {
        if (source == null) {
            return;
        }

        source.stopSubscribe();
        sources.remove(source);
    }

    public int sourceSize() {
        return sources.size();
    }

    public OptionalDouble getBestBidPrice(final int securityId) {
        final Map<ReferencePriceSource, Double> map = securityPriceMap.get(securityId);

        if (map == null || map.size() == 0) {
            return OptionalDouble.empty();
        }

        return OptionalDouble.of(Collections.max(map.values()));
    }

    public OptionalDouble getBestAskPrice(final int securityId) {
        final Map<ReferencePriceSource, Double> map = securityPriceMap.get(securityId);

        if (map == null || map.size() == 0) {
            return OptionalDouble.empty();
        }

        return OptionalDouble.of(Collections.min(map.values()));
    }

    @Override
    public OptionalDouble getBestPrice(final int securityId, final boolean buy) {
        return buy ? getBestAskPrice(securityId) : getBestBidPrice(securityId);
    }
}
