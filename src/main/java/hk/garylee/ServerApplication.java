package hk.garylee;

import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;
import hk.garylee.marketmarker.server.MarketMakerServer;
import hk.garylee.marketmarker.server.MarketMakerServerConfig;
import hk.garylee.service.quotecalculation.QuoteCalculationEngineImpl;
import hk.garylee.service.quotecalculation.QuoteCalculationServiceImpl;
import hk.garylee.service.referenceprice.ReferencePriceServiceImpl;

import java.io.IOException;
import java.net.InetSocketAddress;

public class ServerApplication {

    private static ReferencePriceSource createDummyReferencePriceSource(final int sourceId, final int securityId, final double price) {
        return new ReferencePriceSource() {
            @Override
            public void subscribe(final ReferencePriceSourceListener listener) {
                listener.referencePriceChanged(securityId, price);
            }

            @Override
            public void stopSubscribe() {

            }

            @Override
            public double get(final int securityId) {
                return 0;
            }

            @Override
            public int getSourceId() {
                return securityId;
            }
        };
    }

    public static void main(String[] args) throws IOException {
        final ReferencePriceServiceImpl referencePriceService = new ReferencePriceServiceImpl();
        referencePriceService.addPriceSource(createDummyReferencePriceSource(1, 123, 99.9));

        final QuoteCalculationServiceImpl quoteCalculationService = new QuoteCalculationServiceImpl(new QuoteCalculationEngineImpl(0.05), referencePriceService);

        final MarketMakerServerConfig config = new MarketMakerServerConfig(new InetSocketAddress("localhost", 8000));
        config.setQuoteCalculationService(quoteCalculationService);

        MarketMakerServer server = MarketMakerServer.create(config);
        server.start();
    }
}
