package com.example.marketmaker;

/**
 * Source for reference prices.
 */
public interface ReferencePriceSource {
    /**
     * Subscribe to changes to refernce prices.
     *
     * @param listener callback interface for changes
     */
    void subscribe(ReferencePriceSourceListener listener);

    /**
     * Stop subscribing from change
     */
    void stopSubscribe();

    double get(int securityId);

    /**
     * Retrieve source Id
     * @return source Id
     */
    int getSourceId();
}
