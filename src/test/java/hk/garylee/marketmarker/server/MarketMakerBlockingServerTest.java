package hk.garylee.marketmarker.server;

import hk.garylee.ClientApplication;
import hk.garylee.service.quotecalculation.QuoteCalculationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.util.OptionalDouble;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class MarketMakerBlockingServerTest {
    private final static int PORT = 9000;

    private MarketMakerServerConfig config;
    private MarketMakerBlockingServer cut;

    private QuoteCalculationService quoteCalculationService;

    @Before
    public void before() throws IOException {
        quoteCalculationService = mock(QuoteCalculationService.class);
        when(quoteCalculationService.calculateQuotePrice(123, true, 1000)).thenReturn(OptionalDouble.of(100.99));

        config = new MarketMakerServerConfig(new InetSocketAddress("localhost", PORT));
        config.setQuoteCalculationService(quoteCalculationService);

        cut = new MarketMakerBlockingServer(config);
        cut.start();
    }

    @Test
    public void clientConnectTest() throws IOException, InterruptedException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(byteArrayOutputStream));

        ClientApplication clientApplication = spy(new ClientApplication("localhost", PORT));
        clientApplication.send("123 BUY 1000");

        Thread.sleep(1000);
        Assert.assertEquals("100.99" + System.lineSeparator(), byteArrayOutputStream.toString());
    }
}
