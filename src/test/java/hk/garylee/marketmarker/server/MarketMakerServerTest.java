package hk.garylee.marketmarker.server;

import com.example.marketmaker.QuoteCalculationEngine;
import hk.garylee.service.quotecalculation.QuoteCalculationService;
import hk.garylee.service.quotecalculation.QuoteCalculationServiceImpl;
import hk.garylee.service.referenceprice.ReferencePriceService;
import org.junit.Assert;
import org.junit.Test;

import java.util.OptionalDouble;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MarketMakerServerTest {
    private static final double DELTA = 1e-8;

    @Test
    public void processTest() throws InterruptedException, ExecutionException, TimeoutException {
        QuoteCalculationService quoteCalculationService = mock(QuoteCalculationService.class);
        when(quoteCalculationService.calculateQuotePrice(123, true, 1)).thenReturn(OptionalDouble.of(1000.99));

        CompletableFuture<OptionalDouble> future = MarketMakerServer.processLine(quoteCalculationService, "123 BUY 1".getBytes());
        OptionalDouble price = future.get(10, TimeUnit.SECONDS);

        Assert.assertTrue(future.isDone());
        Assert.assertTrue(price.isPresent());
        Assert.assertEquals(1000.99, price.getAsDouble(), DELTA);
    }

    @Test
    public void processNullTest() {
        MarketMakerServer.processLine(null, null);
        MarketMakerServer.processLine(null, new byte[0]);
        MarketMakerServer.processLine(new QuoteCalculationServiceImpl(mock(QuoteCalculationEngine.class), mock(ReferencePriceService.class)), null);
        MarketMakerServer.processLine(new QuoteCalculationServiceImpl(mock(QuoteCalculationEngine.class), mock(ReferencePriceService.class)), new byte[0]);
    }
}
