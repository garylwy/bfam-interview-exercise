package hk.garylee.service.quotecalcuation;

import hk.garylee.service.quotecalculation.QuoteCalculationEngineImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class QuoteCalculationEngineImplTest {
    private static final double DELTA = 1e-8;
    private QuoteCalculationEngineImpl cut;

    @Before
    public void before() {
        cut = new QuoteCalculationEngineImpl(0.05);
    }

    @Test
    public void calSellPriceTest() {
        final double price = 100.10;
        final double actual = cut.calculateQuotePrice(123, price, false, 1000);
        Assert.assertEquals(95.095, actual, DELTA);
    }

    @Test
    public void calBuylPriceTest() {
        final double price = 100.10;
        final double actual = cut.calculateQuotePrice(123, price, true, 1000);
        Assert.assertEquals(105.105, actual, DELTA);
    }

    @Test
    public void getMarkupTest() {
        Assert.assertEquals(0.05, cut.getMarkup(), DELTA);
    }
}
