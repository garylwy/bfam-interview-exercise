package hk.garylee.service.quotecalcuation;

import com.example.marketmaker.QuoteCalculationEngine;
import hk.garylee.service.quotecalculation.QuoteCalculationEngineImpl;
import hk.garylee.service.quotecalculation.QuoteCalculationServiceImpl;
import hk.garylee.service.referenceprice.ReferencePriceService;
import hk.garylee.service.referenceprice.ReferencePriceServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.OptionalDouble;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class QuoteCalculationServiceImplTest {
    private static final double DELTA = 1e-8;
    private QuoteCalculationEngine quoteCalculationEngine;
    private ReferencePriceService referencePriceService;
    private QuoteCalculationServiceImpl cut;
    
    @Before
    public void before() {
        quoteCalculationEngine = new QuoteCalculationEngineImpl(0.05);
        referencePriceService = spy(new ReferencePriceServiceImpl());
        when(referencePriceService.getBestBidPrice(123)).thenReturn(OptionalDouble.of(99.9));
        when(referencePriceService.getBestAskPrice(123)).thenReturn(OptionalDouble.of(100.1));
        when(referencePriceService.getBestBidPrice(456)).thenReturn(OptionalDouble.empty());
        when(referencePriceService.getBestAskPrice(456)).thenReturn(OptionalDouble.empty());
        
        cut = new QuoteCalculationServiceImpl(quoteCalculationEngine, referencePriceService);
    }
    
    @Test
    public void calculateQuotePriceForBuyTest() {
        final OptionalDouble price = cut.calculateQuotePrice(123, true, 1000);
        Assert.assertTrue(price.isPresent());
        Assert.assertEquals(105.105, price.getAsDouble(), DELTA);
    }

    @Test
    public void calculateQuotePriceForSellTest() {
        final OptionalDouble price = cut.calculateQuotePrice(123, false, 1000);
        Assert.assertTrue(price.isPresent());
        Assert.assertEquals(94.905, price.getAsDouble(), DELTA);
    }

    @Test
    public void calculateQuotePriceNotExistTest() {
        Assert.assertFalse(cut.calculateQuotePrice(456, true, 1000).isPresent());
        Assert.assertFalse(cut.calculateQuotePrice(456, false, 1000).isPresent());
    }
}
