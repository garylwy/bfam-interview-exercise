package hk.garylee.service.referenceprice;

import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.OptionalDouble;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ReferencePriceServiceImplTest {
    private static final double DELTA = 1e-8;
    private ReferencePriceServiceImpl cut;
    
    private static ReferencePriceSource creeateAndAddSource(final ReferencePriceServiceImpl referencePriceService, int securityId, double price) {
        final ReferencePriceSource source = mock(ReferencePriceSource.class);
        final ArgumentCaptor<ReferencePriceSourceListener> sourceListener = ArgumentCaptor.forClass(ReferencePriceSourceListener.class);
        doNothing().when(source).subscribe(sourceListener.capture());
        
        referencePriceService.addPriceSource(source);
        sourceListener.getValue().referencePriceChanged(securityId, price);
        
        return source;
    }

    @Before
    public void before() {
        cut = new ReferencePriceServiceImpl();
    }

    @Test
    public void addPriceSourceNullTest() {
        cut.addPriceSource(null);
        Assert.assertEquals(0, cut.sourceSize());
    }

    @Test
    public void addPriceSourceTest() {
        creeateAndAddSource(cut, 123, 99.99);
        Assert.assertEquals(1, cut.sourceSize());
    }

    @Test
    public void removePriceSourcesTest() {
        final ReferencePriceSource source = creeateAndAddSource(cut, 123, 99.99);
        cut.removePriceSource(source);
        verify(source, times(1)).stopSubscribe();
        Assert.assertEquals(0, cut.sourceSize());
    }

    @Test
    public void removePriceSourcesNullTest() {
        cut.removePriceSource(null);
    }

    @Test
    public void getBestPriceNotExistTest() {
        Assert.assertFalse(cut.getBestPrice(123, true).isPresent());
        Assert.assertFalse(cut.getBestPrice(123, false).isPresent());
    }

    @Test
    public void getBestPriceExistTest() {
        creeateAndAddSource(cut, 123, 100.01);
        creeateAndAddSource(cut, 123, 99.99);

        final OptionalDouble bestBuyPrice = cut.getBestPrice(123, true);
        Assert.assertTrue(bestBuyPrice.isPresent());
        Assert.assertEquals(99.99, bestBuyPrice.getAsDouble(), DELTA);

        final OptionalDouble bestSellPrice = cut.getBestPrice(123, false);
        Assert.assertTrue(bestSellPrice.isPresent());
        Assert.assertEquals(100.01, bestSellPrice.getAsDouble(), DELTA);
    }
}
