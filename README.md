# Exercise Submission
(c) Gary Lee

This project used `lombok` for generating getter and setter. Please install corresponding plugin in your IDE.

This submission have two implementation of MarketMarketServer:
- `MarketMakerBlockingServer` is based on blocking I/O, which is "one-thread-pre-connection" and good for small number of client.
- `MarketMakerNonBlockingServer` is based on Non-blocking I/O, which is using Selector to manage multiple socket connection. It is good for large number of connection. 

 `MarketMakerServer` used `ForkJoinPool.common()` for CPU-bounded processing. By default, the number of thread in `common` pool is limited by number of CPU core minus 1.

## `QuoteCalculationEngine`
For simplicity, the implementation of `QuoteCalculationEngine` is applying markup/markdown on reference price. It return `-1` if reference price not available.

## `ReferencePriceServiceImplV2`
This implementation demonstrated data structure for price update with `O(1)` or `O(log m)` time complexity and retrieve best price in `O(1)`, where `m` is number of price. However, it is not thread-safety. But it can be, if speed some effort.

This should have better performance in the case of having large number of source.

## Local run
- Run `hk.garylee.ServerApplication`, it start the server on `8000` port
- For testing, it create a dummy ReferencePriceSource with sourceId `1`, securityId `123` and price `99.9`
- Run `hk.garylee.ClientApplication`, it connect to `localhost:8000`
- After connection established, user can interact with server via console input. For example: `123 BUY 1000`, `123 SELL 2000`, `0005 BUY 400`
